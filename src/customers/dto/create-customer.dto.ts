import { IsNotEmpty, Length, Matches } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  @Length(0, 10)
  tel: string;

  @IsNotEmpty()
  gender: string;
}
